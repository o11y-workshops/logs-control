Logs Control Easy Install
=========================
This demo automates the deployment of a four node Kubernetes cluster with Kind 
and setting up the Ghost CMS platform to generate logs for this demo. It delivers 
a ready to use Kubernetes log generation demo. The installation scripts check for 
and validate prerequisites automatically.

Install the demo
----------------
This is an installation using Podman, Kind, and Ghost to run the demo on a 
virutal machine. Note that any missing requirements will be reported by the
installation script and point you to the necessary actions to fix.

**Minium requirements:** Podman 5.x+ with your podman machine started, 
Kind 0.27.x+, Kubectl 1.32.x+, and Helm 3.x+.

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/logs-control/-/archive/v1.0/logs-control-v1.0.zip)

2. Run 'init.sh' from the project directory to create a Kubernetes cluster using Podman 
   container platform and install the Ghost blogging platform to generate logs. 

```
   $ ./init.sh 
```

3. Follow the console output for connecting to the Ghost platform and the login
   credentials for your cluster. 

<img src="docs/demo-images/ghost-install.png" width="70%">

Notes:
-----

 - Running the installation script the first time with a new Podman virtual
   machine means that the Kind cluster image needs to download. The second time
   you run the installation, that will be cached and create the cluster much
   faster.

 - Creating a Podman machine with more memory helps speed up the deployment of
   your workloads. With a default of 2048 memory, you can double it to deploy
   the Ghost workload in ~2 minutes:

   ```
     $ podman machine init --memory 4096

     $ podman machine start
   ```

  - If for any reason the installation breaks or you want a new logs control
    install dmeo, just stop the demo with CTRl-C, remove the cluster and 
    reinstall using the installation script as follows:

   ```
     $ kind --name=4node delete cluster

     Deleting cluster "4node" ...
     Deleted nodes: ["4node-worker2" "4node-worker3" "4node-worker" "4node-control-plane"]

     $ ./init.sh
   ``` 

Supporting Articles
-------------------
Coming soon...


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.0 - Supporting Podman v5.x+, Kind 0.27.x+, Kubectl 1.32.x+, and Helm 3.x+ installing Kubernetes cluster with Ghost platform generating logs. 

