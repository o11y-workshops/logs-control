#!/bin/bash

DEMO="Logs control easy install"
AUTHORS="Bala RameshBabu, Eric D. Schabell"
PROJECT="git@gitlab.com:o11y-workshops/logs-control.git"

# variables used globally in sourced functions.
export MAJ_PODMAN_VERSION=5
export KIND_MAJ_VERSION=0
export KIND_MINOR_VERSION=27
export GHOST_MAJ_VERSION=21
export MAJ_HELM_VERSION=3
export KUBECTL_MAJ_VERSION="1"
export KUBECTL_MINOR_VERSION="32"
export USERNAME="adminuser"
export EMAIL_ADDRESS="admin@example.com"
export SUPPORT_DIR=support
export LOGS_HOME=target
export HELM_BIN=${LOGS_HOME}/get_helm.sh
export KINDCONFIG="${PWD}/${SUPPORT_DIR}/4nodekindconfig.yaml"
export KUBECONFIG="${PWD}/${SUPPORT_DIR}/4nodeconfig.yaml"
export GHOST_PVS="${PWD}/${SUPPORT_DIR}/ghost-static-pvs.yaml"

# adjust these variables as desired
export KIND_CLUSTER_NAME=4node
export KIND_CONTEXT=kind-4node

# wipe screen.
clear

echo
echo "#######################################################"
echo "##                                                   ##"
echo "##  Setting up the ${DEMO}         ##"
echo "##                                                   ##"
echo "##              #     ##### #####  ####              ##"
echo "##              #     #   # #     #                  ##"
echo "##              #     #   # # ###  ###               ##"
echo "##              #     #   # #   #     #              ##"
echo "##              ##### ##### ##### ####               ##"
echo "##                                                   ##"
echo "##      ##### ##### #   # ##### ####  ##### #        ##"
echo "##      #     #   # ##  #   #   #   # #   # #        ##"
echo "##      #     #   # # # #   #   ##### #   # #        ##"
echo "##      #     #   # #  ##   #   #  #  #   # #        ##"
echo "##      ##### ##### #   #   #   #   # ##### #####    ##"
echo "##                                                   ##"
echo "##      ##### #   #  #### #####  ###  #     #        ##"
echo "##        #   ##  # #       #   #   # #     #        ##"
echo "##        #   # # #  ###    #   ##### #     #        ##"
echo "##        #   #  ##     #   #   #   # #     #        ##"
echo "##      ##### #   # ####    #   #   # ##### #####    ##"
echo "##                                                   ##"
echo "##  brought to you by,                               ##"
echo "##          ${AUTHORS}        ##"
echo "##                                                   ##"
echo "##  ${PROJECT}   ##"
echo "##                                                   ##"
echo "#######################################################"
echo

# Check the podman installation.
echo "Checking if Podman is installed..."
echo
command -v podman --version -v  >/dev/null 2>&1 || { echo >&2 "Podman is required but not installed yet... download and install: https://podman.io/getting-started/installation"; exit; }

echo "Checking for Podman version..."
echo
maj_version=$(podman --version | cut -d" " -f3 | cut -d "." -f1)

if [ "${maj_version}" -ge "${MAJ_PODMAN_VERSION}" ]; then
  echo "  - installed Podman version is v${maj_version}..."
  echo
else
  echo "Your Podman version is ${maj_version}, it must be ${MAJ_PODMAN_VERSION}.x or higher, please upgrade..."
  exit;
fi

# Check the k8s installation.
echo "Checking if Kind is installed..."
echo
command -v kind version >/dev/null 2>&1 || { echo >&2 "Kind is required but not installed yet... download and install: https://kind.sigs.k8s.io/docs/user/quick-start/"; exit; }

echo "Checking for Kind version..."
echo
maj_version=$(kind version | cut -d" " -f2 | cut -d "." -f1 | cut -d "v" -f2)
minor_version=$(kind version | cut -d" " -f2 | cut -d "." -f2)

if [[ "${maj_version}" -ge "${KIND_MAJ_VERSION}" && "${minor_version}" -ge "${KIND_MINOR_VERSION}" ]]; then
  echo "  - installed Kind version is v${maj_version}.${minor_version}..."
  echo
else
  echo "Your Kind version is ${maj_version}.${minor_verison}, it must be ${KIND_MAJ_VERSION}.${KIND_MINOR_VERSION}.x or higher, please upgrade..."
  exit;
fi

# Check the helm installation.
echo "Checking if Helm is installed..."
echo
command -v helm version --short >/dev/null 2>&1 || { echo >&2 "Helm version 3+ is required but not installed yet... download and install here: https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3"; exit; }

echo "Checking for Helm version..."
echo
maj_version=$(helm version --short | cut -d" " -f3 | cut -d "." -f1 | cut -d "o" -f2 | cut -d "v" -f2)

if [ "${maj_version}" -ge "${MAJ_HELM_VERSION}" ]; then
  echo "  - installed Helm version is v${maj_version}..."
  echo
else
  echo
  echo "Your Helm version is ${maj_version}, it must be ${HELM_VERSION} or higher, please upgrade..."
  exit;
fi

# Check that podman machine is started.
echo "Checking that podman machine is running..."
echo

machine_status=$(podman machine info | grep machinestate | cut -d ':' -f2)
if [[ ${machine_status} != " Running" ]]; then
  echo "Podman machine is not running. Please make sure you have initialized and started the"
  echo "Podman machine as follows and rerun this installation script again. If possible add "
  echo "extra memory to a new Podman virtual machine as follows:"
  echo
  echo "     $ podman machine init --memory 4096"
  echo
  echo "     $ podman machine start"
  echo
  exit;
fi

# Check the kubectl installation.
echo "Checking if kubectl is installed..."
echo
command -v kubectl version >/dev/null 2>&1 || { echo >&2 "Kubectl is required but not installed yet... download and install: https://kubernetes.io/docs/tasks/tools/"; exit; }

echo "Checking for kubectl version..."
echo
maj_version=$(kubectl version | head -n1 | cut -d" " -f3 | cut -d"." -f1 | cut -d"v" -f2)
minor_version=$(kubectl version | head -n1 | cut -d" " -f3 | cut -d"." -f2)

if [[ "${maj_version}" -ge "${KUBECTL_MAJ_VERSION}" && "${minor_version}" -ge "${KUBECTL_MINOR_VERSION}" ]]; then
  echo
  echo "  - installed kubectl version is v${maj_version}.${minor_version}..."
  echo
else
  echo
  echo "Your kubectl version is ${maj_version}.${minor_verison}, it must be ${KUBECTL_MAJ_VERSION}.${KUBECTL_MINOR_VERSION}.x or higher, please upgrade..."
  exit;
fi

echo "Starting Kubernetes cluster deployment..."
echo
if [ -x "$LOGS_HOME" ]; then
  echo "  - removing existing installation directory..."
  echo
  command rm -rf ${LOGS_HOME}
fi

echo "Creating directories for Kubernetes control and worker nodes..."
echo 
command mkdir -p "${LOGS_HOME}/ctrlnode"

if [ $? -ne 0 ]; then
  echo
  echo "Error occurred during creation of ${LOGS_HOME}/ctrlnode"
  echo
  exit;
fi

for i in $(seq 1 3)
do
  command mkdir -p "${LOGS_HOME}/wrkrnode${i}"

  if [ $? -ne 0 ]; then
    echo
	  echo "Error occurred during creation of ${LOGS_HOME}/wrknode${i}"
	  echo
	  exit;
  fi
done

echo "Removing any previous k8s cluster named ${KIND_CLUSTER_NAME}..."
echo
command -v `KIND_EXPERIMENTAL_PROVIDER=podman kind --name="${KIND_CLUSTER_NAME}" delete cluster`

if [ $? -ne 0 ]; then
  echo
  echo "Problem trying to remove existing cluster 'podman kind --name=${KIND_CLUSTER_NAME} delete cluster'..."
  echo
  exit;
fi

echo
echo "Starting the new cluster named ${KIND_CLUSTER_NAME}..."
echo
command `KIND_EXPERIMENTAL_PROVIDER=podman kind create cluster --name="${KIND_CLUSTER_NAME}" --config="${KINDCONFIG}" --retain`

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during spin up of k8s cluster 6node."
	echo
	exit;
fi

echo "Kubernetes cluster creation successful..."
echo
command kubectl --kubeconfig "${KUBECONFIG}" config view

echo
echo "Setting context to uese ${KIND_CONTEXT}..."
echo
command kubectl --kubeconfig "${KUBECONFIG}" config use-context "${KIND_CONTEXT}"

echo
echo "Creating persistent volumes for Ghost workload..."
echo
command kubectl create -f "${GHOST_PVS}" --validate=false

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during creation of persistent volumes for Ghost workload."
	echo
	exit;
fi

echo
echo "Creating namespace for Ghost workload..."
echo
command kubectl --kubeconfig "${KUBECONFIG}" create ns ghost

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during creation of Ghost workload namespace."
	echo
	exit;
fi

echo
echo "Adding Ghost helm chart to our local repo..."
echo
command helm repo add bitnami https://charts.bitnami.com/bitnami

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during adding Ghost to helm repo."
	echo
	exit;
fi


echo
echo "Deploying Ghost workload with Helm chart..."
echo
command helm install ghost-dep bitnami/ghost --version 21.1.15 --namespace=ghost --set ghostUsername="${USERNAME}" --set ghostEmail="${EMAIL_ADDRESS}" --set service.type=ClusterIP --set service.ports.http=2368 >/dev/null 2>&1 

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during deployment Ghost with Helm chart."
	echo
	exit;
fi

echo "Confirming deployments have completed successfully on the cluster... it takes a few minutes..."
echo
command kubectl --kubeconfig "${KUBECONFIG}" wait --for=condition=Ready pod --all --timeout=200s --namespace ghost

echo
echo "The status of all Ghost pods in the cluster are:"
echo
command kubectl --kubeconfig "${KUBECONFIG}" get pods --namespace ghost

echo
echo "Forwarding the necessary Ghost port 2368 for UI..."
echo
command kubectl --kubeconfig "${KUBECONFIG}" port-forward --namespace ghost svc/ghost-dep 2368:2368 >/dev/null 2>&1 &

echo "Retrieving username and password for Ghost platform login..."
echo
PASSWORD=$(kubectl --kubeconfig "${KUBECONFIG}" get secret --namespace ghost ghost-dep -o jsonpath="{.data.ghost-password}" | base64 -d)

echo "======================================================"
echo "=                                                    ="
echo "=  Install complete, Ghost platform available on     ="
echo "=  a Kubernetes 4 node cluster generating logs.      ="
echo "=                                                    ="
echo "=  The Ghost blog platform can be viewed at:         ="
echo "=                                                    ="
echo "=    http://localhost:2368                           ="
echo "=    http://localhost:2368/ghost  (admin panel)      ="
echo "=                                                    ="
echo "=    Username: ${EMAIL_ADDRESS}                     ="
echo "=    Password: ${PASSWORD}                            ="
echo "=                                                    ="
echo "=  Note: When finished using the demo setup,         ="
echo "=  you can shut it down and come back later to       ="
echo "=  restart it:                                       ="
echo "=                                                    ="
echo "=    $ podman machine stop                           ="
echo "=                                                    ="
echo "=  To clean up the cluster entirely either rerun     ="
echo "=  the installation script to cleanup existing       ="
echo "=  cluster and reinstall a new Ghost platform, or    ="
echo "=  run the following command:                        ="
echo "=                                                    ="
echo "=    $ kind --name=${KIND_CLUSTER_NAME} delete cluster              ="
echo "=                                                    ="
echo "======================================================"
echo
